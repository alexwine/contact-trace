# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.4.0](https://gitlab.com/alexwine/contact-trace/compare/v1.3.0...v1.4.0) (2020-10-28)


### Features

* **contact required fields:** option to make phone and email required ([a765faa](https://gitlab.com/alexwine/contact-trace/commit/a765faa2e9979eef861a91ca720ed95eefdf8098))


### Bug Fixes

* **migration:** removed manager group migration ([d13d1a5](https://gitlab.com/alexwine/contact-trace/commit/d13d1a51f52a1077c3b5ae0d62b87e88991eb245))

## [1.3.0](https://gitlab.com/alexwine/contact-trace/compare/v1.2.0...v1.3.0) (2020-10-26)


### Features

* **pages:** added dynamic pages for home and about ([3051924](https://gitlab.com/alexwine/contact-trace/commit/3051924a53814871afe489efdf6e46a1ecf28098))
* **perms:** updated to use Guardian Permissions ([e79ec51](https://gitlab.com/alexwine/contact-trace/commit/e79ec51c69b3ff0668620334d28603718c9ee43f))


### Bug Fixes

* **company.contacts:** changed max display to 20 ([6e7f53b](https://gitlab.com/alexwine/contact-trace/commit/6e7f53b3fc175925934bb5fdeda0dba59f2c5bfc))
* **menu icon:** potential fix for disappearing menu icon ([f343264](https://gitlab.com/alexwine/contact-trace/commit/f343264f3f5a842a1a2200e1e6173ad3421134f7))
* **signal:** changed user signal implementation ([f9424fa](https://gitlab.com/alexwine/contact-trace/commit/f9424fa93b42a8b4147de8f375c9db92a7a58f12))


### CI

* **test:coverage:** added migrate before test:coverage ([74adaf3](https://gitlab.com/alexwine/contact-trace/commit/74adaf3dbf6e2da075dcd935a9c7aa9be047a24c))

## [1.2.0](https://gitlab.com/alexwine/contact-trace/compare/v1.1.0...v1.2.0) (2020-10-26)


### Features

* **footer:** added support footer for emailing support requests ([be41cf3](https://gitlab.com/alexwine/contact-trace/commit/be41cf360423d5b2dbc4a670ab22f6d43d5edea8))


### Bug Fixes

* **lint:** fixed lint errors ([209e975](https://gitlab.com/alexwine/contact-trace/commit/209e9757f347e2d74c621bf2b76e4111a4621e31))

## [1.1.0](https://gitlab.com/alexwine/contact-trace/compare/v1.0.1...v1.1.0) (2020-10-26)


### Features

* **pusher:** added web sockets for new contacts added ([0c76cbf](https://gitlab.com/alexwine/contact-trace/commit/0c76cbf22011d6951de6c26cde88836f3bda0625)), closes [#1](https://gitlab.com/alexwine/contact-trace/issues/1)


### Bug Fixes

* **release:** updated gitlab release command ([1c0a8c4](https://gitlab.com/alexwine/contact-trace/commit/1c0a8c423607edd5e621b95e0854f3933e3dec25))


### CI

* **pages:** fixed docs path ([a3a1e14](https://gitlab.com/alexwine/contact-trace/commit/a3a1e14da2e4ea80c9c93cc97bf3889f05a49d35))

### 1.0.1 (2020-10-25)


### Bug Fixes

* **package.json:** updated version in package.json ([6aa3979](https://gitlab.com/alexwine/contact-trace/commit/6aa3979d5538c856b83cc9e0349e1b35e636ad40))


### Tests

* **test_tasks:** fixed failing test tasks ([fe893c6](https://gitlab.com/alexwine/contact-trace/commit/fe893c6b4e1aecdf0ef4ac7912e9890022189062))


### CI

* **before script:** added pip install before script ([607be30](https://gitlab.com/alexwine/contact-trace/commit/607be304dd66e21e461ed28c5d14d2b197dac5ca))
* **before script:** changed from global before_script ([874c217](https://gitlab.com/alexwine/contact-trace/commit/874c21743d459004177724e07d9d2368a034ef00))
* **deploy:** updated image reference to python ([a176d23](https://gitlab.com/alexwine/contact-trace/commit/a176d23b98a0e9e1dfa6a43b1c7581a0842653bd))
* **docs:** updated pages for docs display on GitLab Pages ([63b046b](https://gitlab.com/alexwine/contact-trace/commit/63b046bee663d5fa64a8f10b6a21761595509643))
* **services:** added correct postgres db url ([edc6530](https://gitlab.com/alexwine/contact-trace/commit/edc6530e4a2516bfdcb716dece2fb4833155314a))
* **services:** added postgres for testing application ([bccea62](https://gitlab.com/alexwine/contact-trace/commit/bccea623bd99ccd1ef99a799ef291bbd6ab336e5))
* **test:coverage:** fixed pytest with coverage ([32912fa](https://gitlab.com/alexwine/contact-trace/commit/32912fa76df44d3572ed6d9dcb1d79bcb87ddf13))
