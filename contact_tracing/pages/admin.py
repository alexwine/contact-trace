from django.contrib import admin
from guardian.admin import GuardedModelAdmin

from .models import Page


# Register your models here.
@admin.register(Page)
class PageAdming(GuardedModelAdmin):
    exclude = ["content"]
