from django.apps import AppConfig


class PagesConfig(AppConfig):
    name = "contact_tracing.pages"
