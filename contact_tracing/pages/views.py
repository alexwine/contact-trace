from django.shortcuts import get_object_or_404, render
from django.views.generic.detail import DetailView

from .models import Page


# Create your views here.
class PageDetailView(DetailView):

    model = Page
    slug_field = "slug"
    slug_url_kwarg = "slug"

    template_name = "pages/detail.html"


page_detail_view = PageDetailView.as_view()


def home_page(request):
    page = get_object_or_404(Page, pk=1)
    context = {"page": page}
    return render(request, "pages/detail.html", context)


def about_page(request):
    page = get_object_or_404(Page, pk=2)
    context = {"page": page}
    return render(request, "pages/detail.html", context)
