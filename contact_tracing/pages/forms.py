from crispy_forms.helper import FormHelper
from django import forms
from tinymce.widgets import TinyMCE

from .models import Page


class PageForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = "id-exampleForm"

    name = forms.CharField(label="Name", required=True)
    content = forms.CharField(widget=TinyMCE(attrs={"cols": 80, "rows": 30}))

    class Meta:
        model = Page
