from django.urls import path

from contact_tracing.pages.views import page_detail_view

app_name = "users"
urlpatterns = [
    # path("~redirect/", view=user_redirect_view, name="redirect"),
    # path("~update/", view=user_update_view, name="update"),
    path("<str:slug>/", view=page_detail_view, name="detail"),
]
