from django.db import models
from tinymce import models as tinymce_models


# Create your models here.
class Page(models.Model):
    name = models.CharField(max_length=200)
    data = tinymce_models.HTMLField()
    slug = models.SlugField()
    jumbo = models.BooleanField(default=True)
    bg_image = models.URLField(
        verbose_name="Background Image",
        default="https://images.unsplash.com/photo-1555396273-367ea4eb4db5?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=1000&amp;q=80",  # noqa: E501
    )

    def __str__(self):
        return self.name
