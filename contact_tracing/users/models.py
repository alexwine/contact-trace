from django.contrib.auth.models import AbstractUser, Permission
from django.db.models import CharField
from django.db.models.deletion import DO_NOTHING
from django.db.models.fields.related import ForeignKey
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from guardian.shortcuts import assign_perm

from contact_tracing.companies.models import Company


class User(AbstractUser):
    """Default user for Contact Tracing."""

    #: First and last name do not cover name patterns around the globe
    name = CharField(_("Name of User"), blank=True, max_length=255)
    company = ForeignKey(Company, on_delete=DO_NOTHING, null=True)

    def get_absolute_url(self):
        """Get url for user's detail view.

        Returns:
            str: URL for user detail.

        """
        return reverse("users:detail", kwargs={"username": self.username})


# def check_iterable(obj):
#     try:
#         iterator = iter(obj)
#     except TypeError:
#         return False
#     else:
#         return True


@receiver(post_save, sender=User)
def user_post_save_receiver(sender, **kwargs):

    user, created = kwargs["instance"], kwargs["created"]
    print(user, created, sender, kwargs)
    if not created:
        perm = Permission.objects.get(codename="view_company_contacts")

        company = user.company
        if company:
            # group = Group.objects.get(name=f"company_{company.id}")

            # if group in user.groups.all():
            #     pass
            # # if check_iterable(group):
            # else:
            #     print("Assigned Permission")
            #     user.groups.add(group)
            #     user.save()
            assign_perm(perm, user, company)
