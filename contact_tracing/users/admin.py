from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model
from django.db.models import Q

from contact_tracing.companies.models import Company
from contact_tracing.users.forms import UserChangeForm, UserCreationForm

User = get_user_model()


@admin.register(User)
class UserAdmin(auth_admin.UserAdmin):
    # def get_queryset(self, request):
    #     qs = super().get_queryset(request)
    #     if request.user.is_superuser:
    #         return qs
    #     return qs.filter(company=request.user.company)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        filter_data = Q(company=request.user.company) | Q(company=None)
        return (
            qs.exclude(username__iexact="AnonymousUser")
            .filter(filter_data)
            .exclude(is_superuser=True)
        )

    form = UserChangeForm
    add_form = UserCreationForm

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if request.user.is_superuser:
            return super().formfield_for_foreignkey(db_field, request, **kwargs)
        if db_field.name == "company":
            kwargs["queryset"] = Company.objects.filter(id=request.user.company.id)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_fieldsets(self, request, obj):
        if request.user.is_superuser:
            return (("User", {"fields": ("name", "company")}),) + tuple(
                auth_admin.UserAdmin.fieldsets
            )
        return (("User", {"fields": ("name", "company")}),) + (
            # (None, {'fields': ('username', 'password')}),
            (("Personal info"), {"fields": ("first_name", "last_name")}),
            (
                ("Permissions"),
                {
                    "fields": ("is_staff", "groups"),
                },
            ),
        )

    readonly_fields = ["email", "username"]

    list_display = ["username", "name", "is_superuser"]
    search_fields = ["name"]
