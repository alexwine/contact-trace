import datetime

import pusher
from colorfield.fields import ColorField
from django.conf import settings
from django.contrib.auth.models import Group, Permission
from django.db import models
from django.db.models.fields.related import ForeignKey
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from guardian.shortcuts import assign_perm


# Create your models here.
class Company(models.Model):
    name = models.CharField(max_length=200)
    location = models.CharField(max_length=200, default="", blank=True)
    image = models.URLField(blank=True)
    brand_color = ColorField(default="#c4dce8")
    button_color = ColorField(default="#007bff")
    background_color = ColorField(default="#007bff")
    button_text_color = ColorField(default="#fff")
    outline_button = models.BooleanField(default=False)
    phone_number_required = models.BooleanField(default=True)
    email_required = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = "Companies"
        permissions = [
            ("view_company_contacts", "Can view company contacts"),
            # ("close_task", "Can remove a task by setting its status as closed"),
        ]

    def __str__(self) -> str:
        return self.name


class ContactManager(models.Manager):
    def create_contact(self, company, name, phone_number, email):
        contact = Contact(
            name=name, phone_number=phone_number, company=company, email=email
        )
        contact.save()
        print(contact)
        # do something with the contact
        return contact


class Contact(models.Model):
    name = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    company = ForeignKey(Company, on_delete=models.DO_NOTHING)
    phone_number = models.CharField(max_length=20, default="")
    email = models.EmailField(default="")
    objects = ContactManager()

    def __str__(self) -> str:
        return self.name

    class Meta:
        ordering = ["-created_at"]

    def was_added_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(weeks=3) <= self.created_at <= now

    was_added_recently.admin_order_field = "created_at"
    was_added_recently.boolean = True
    was_added_recently.short_description = "Within three weeks?"


pusher_client = pusher.Pusher(
    app_id=settings.PUSHER_APP_ID,
    key="dd24f028ebf4c08149d2",
    secret=settings.PUSHER_SECRET,
    cluster="us3",
    ssl=True,
)


def get_permission(req):
    perm = Permission.objects.get(codename=req)
    return perm


@receiver(post_save, sender=Company)
def company_post_save_receiver(sender, **kwargs):
    company = kwargs["instance"]
    # if created:
    group, crea = Group.objects.get_or_create(name=f"company_{company.id}")
    print(crea)
    # group = Group.objects.create(name=f"company_{company.id}")
    # Employee Permissions
    assign_perm(get_permission("view_company_contacts"), group, company)
    assign_perm(get_permission("change_company"), group, company)
    assign_perm(get_permission("view_company"), group, company)
    # Manager Permissions
    # assign_perm(get_permission('change_company'), manager_group, company)
    # assign_perm(get_permission('change_user'), manager_group, company)


@receiver(post_save, sender=Contact)
def contact_post_save_receiver(sender, **kwargs):
    contact, created = kwargs["instance"], kwargs["created"]
    print(contact, created, sender, kwargs)
    pusher_client.trigger(
        "contact-added",
        str(contact.company.id),
        {
            "name": contact.name,
            "email": contact.email,
            "phone_number": contact.phone_number,
            "id": contact.id,
        },
    )
