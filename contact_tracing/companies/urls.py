from django.urls import path

from contact_tracing.companies.views import company_detail_view, contact_create_view

app_name = "companies"
urlpatterns = [
    # path("~redirect/", view=user_redirect_view, name="redirect"),
    path("<int:pk>/~create/", view=contact_create_view, name="contacts-create"),
    path("<int:pk>/", view=company_detail_view, name="detail"),
]
