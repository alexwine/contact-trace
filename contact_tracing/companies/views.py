from colormap import hls2rgb, rgb2hex, rgb2hls
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView
from guardian.mixins import PermissionRequiredMixin

from contact_tracing.companies.forms import AddContactForm

from .models import Company, Contact


class CompanyDetailView(PermissionRequiredMixin, DetailView):
    model = Company

    template_name = "companies/detail.html"
    permission_required = "companies.view_company_contacts"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)

        path = self.request.get_full_path() + "~create/"
        path = self.request.build_absolute_uri(path)
        print(path)
        context["create_link"] = path
        context["contacts"] = context["company"].contact_set.all()[:20]
        return context


company_detail_view = CompanyDetailView.as_view()


def get_companypk(path):
    numbers = []
    for word in path.split("/"):
        if word.isdigit():
            numbers.append(int(word))
    if len(numbers) > 0:
        # print(numbers)
        return numbers[0]
    return 0


def hex_to_rgb(hex_color):
    hex_color = hex_color.lstrip("#")
    hlen = len(hex_color)
    return tuple(
        int(hex_color[i : i + hlen // 3], 16)  # noqa: E203
        for i in range(0, hlen, hlen // 3)  # noqa: E203
    )


def adjust_color_lightness(r, g, b, factor):
    h, ls, s = rgb2hls(r / 255.0, g / 255.0, b / 255.0)
    ls = max(min(ls * factor, 1.0), 0.0)
    r, g, b = hls2rgb(h, ls, s)
    return rgb2hex(int(r * 255), int(g * 255), int(b * 255))


def darken_color(r, g, b, factor=0.1):
    return adjust_color_lightness(r, g, b, 1 - factor)


def darken_hex(hex_color, factor=1.15):
    r, g, b = hex_to_rgb(hex_color)
    return adjust_color_lightness(r, g, b, factor)


class ContactCreateView(FormView):
    model = Contact
    form_class = AddContactForm
    template_name = "companies/create_checkin.html"
    # def get_object(self):
    #     return User.objects.get(username=self.request.user.username)
    # fields = ["name", "phone_number"]
    # success_url =

    # def __init__(self) -> None:
    #     numbers = []
    #     for word in self.request.path.split("/"):
    #         if word.isdigit():
    #             numbers.append(int(word))
    #     if len(numbers) > 0:
    #         # print(numbers)
    #         self.primary_key = numbers[0]
    #     self.primary_key = 0

    def get_context_data(self, **kwargs):
        print(self.request)
        pk = get_companypk(self.request.path)

        context = super().get_context_data(**kwargs)
        print("PK", pk)
        company = get_object_or_404(Company, pk=pk)
        button_text_color = company.button_text_color
        button_color = company.button_color
        dark_button_color = darken_hex(company.button_color)
        background_color = company.background_color
        dark_background_color = darken_hex(company.background_color)
        context["company"] = company
        context["button_text_color"] = button_text_color
        context["button_color"] = button_color
        context["dark_button_color"] = dark_button_color
        context["background_color"] = background_color
        context["dark_background_color"] = dark_background_color
        print(dark_button_color)
        print(button_color)
        return context

    def get_success_url(self) -> str:
        print(self.request.path)
        return self.request.path

    def form_valid(self, form):

        self.add_check_in(form.cleaned_data)
        return super().form_valid(form)

    def get_initial(self):
        initial = super(ContactCreateView, self).get_initial()
        pk = get_companypk(self.request.path)
        initial.update({"company_id": pk})
        return initial

    def add_check_in(self, valid_data):
        company_id = valid_data["company_id"]
        name = valid_data["name"]
        phone_number = valid_data["phone_number"]
        email = valid_data["email"]
        company = Company.objects.get(id=company_id)
        Contact.objects.create_contact(company, name, phone_number, email)
        messages.add_message(
            self.request, messages.SUCCESS, "Check in successfully updated!"
        )
        print(valid_data)


contact_create_view = ContactCreateView.as_view()

# def create_checkin(request, company_id):
#     company = get_object_or_404(Company, pk=company_id)

#     if request.method == 'POST':
#         form = AddContactForm(request.POST)

#         if form.is_valid():
