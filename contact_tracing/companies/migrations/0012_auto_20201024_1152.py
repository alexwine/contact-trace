# Generated by Django 3.0.10 on 2020-10-24 17:52

import colorfield.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0011_auto_20201024_1016'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='background_color',
            field=colorfield.fields.ColorField(default='#007bff', max_length=18),
        ),
        migrations.AddField(
            model_name='company',
            name='button_color',
            field=colorfield.fields.ColorField(default='#007bff', max_length=18),
        ),
        migrations.AddField(
            model_name='company',
            name='button_text_color',
            field=colorfield.fields.ColorField(default='#007bff', max_length=18),
        ),
        migrations.AlterField(
            model_name='contact',
            name='phone_number',
            field=models.CharField(default='', max_length=20),
            preserve_default=False,
        ),
    ]
