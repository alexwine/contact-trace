# Generated by Django 3.0.10 on 2020-10-24 16:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0009_contact_email'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='image',
            field=models.ImageField(blank=True, upload_to=''),
        ),
    ]
