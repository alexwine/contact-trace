# Generated by Django 3.0.10 on 2020-10-24 17:52

import colorfield.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0012_auto_20201024_1152'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='button_text_color',
            field=colorfield.fields.ColorField(default='#fff', max_length=18),
        ),
    ]
