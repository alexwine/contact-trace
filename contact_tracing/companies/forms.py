from crispy_forms.helper import FormHelper
from django import forms


class AddContactForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = "id-exampleForm"

    name = forms.CharField(label="Name", required=True)
    phone_number = forms.CharField(label="Phone Number", required=False)
    company_id = forms.IntegerField(widget=forms.HiddenInput())
    email = forms.EmailField(required=False)


# class CompanyForm(forms.ModelForm):
#     # def __init__(self, *args, **kwargs):
#     #     super().__init__(*args, **kwargs)
#     #     self.helper = FormHelper()
#     #     self.helper.form_id = "id-exampleForm"

#     # name = forms.CharField(label="Name", required=True)
#     # phone_number = forms.CharField(
#     #     label="Phone Number",
#     #     # required=False
#     # )
#     # company_id = forms.IntegerField(widget=forms.HiddenInput())
#     model = Company
#     brand_color = forms.CharField(
#         label="Brand Color",
#         # initial="#e66465",
#         widget=TextInput({
#             "type": "color",
#             "value": "#e66465"
#         })

#     )
