from django.contrib import admin
from guardian.admin import GuardedModelAdmin

# from contact_tracing.companies.forms import CompanyForm
from contact_tracing.companies.models import Company, Contact


# Register your models here.
@admin.register(Contact)
class ContactAdmin(GuardedModelAdmin):
    # date_hierarchy = 'created_at'
    # fields = ('name', 'phone_number')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(company=request.user.company)

    readonly_fields = ["company"]
    list_display = ("name", "company", "was_added_recently", "created_at")
    list_filter = ["created_at"]
    search_fields = ["name"]
    fieldsets = [
        (
            "Personal Info",
            {
                "fields": ["name", "phone_number", "email"],
            },
        ),
        ("Meta", {"fields": ["company"], "classes": ["collapse"]}),
    ]

    # admin.site.register(Company)


# admin.site.register(Company)
@admin.register(Company)
class CompanyAdmin(GuardedModelAdmin):
    user_can_access_owned_objects_only = True

    def has_change_permission(self, request, obj=None) -> bool:
        user = request.user
        if obj:
            print(obj.id)
            if user.has_perm("companies.change_company", obj):
                return True
            else:
                return False
        return True

    # form = CompanyForm

    # def get_queryset(self, request):
    #     qs = super().get_queryset(request)
    #     if request.user.is_superuser:
    #         return qs
    #     return qs.filter(id=request.user.company.id)
    exclude = ["brand_color"]
    fieldsets = [
        (
            "Company Info",
            {
                "fields": ["name", "location"],
            },
        ),
        ("Fields", {"fields": ["phone_number_required", "email_required"]}),
        (
            "Display Properties",
            {
                "fields": [
                    "image",
                    "button_color",
                    "background_color",
                    "button_text_color",
                    "outline_button",
                ],
                "classes": ["collapse"],
            },
        ),
    ]
